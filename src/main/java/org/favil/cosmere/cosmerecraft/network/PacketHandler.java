package org.favil.cosmere.cosmerecraft.network;

import java.util.concurrent.atomic.AtomicInteger;

import org.favil.cosmere.cosmerecraft.CosmereCraftMod;
import org.favil.cosmere.cosmerecraft.network.message.MessagePowerSync;
import org.favil.cosmere.cosmerecraft.network.message.MessagePowerSync.MessagePowerSyncHandler;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketHandler {
  public static final SimpleNetworkWrapper INSTANCE =
      NetworkRegistry.INSTANCE.newSimpleChannel(CosmereCraftMod.class.getSimpleName());
  public static final AtomicInteger ID = new AtomicInteger(0);
  
  public static void init() {
    CosmereCraftMod.logger.info("Registering network messages");
    // Register network messages and handlers here.
    INSTANCE.registerMessage(MessagePowerSyncHandler.class, MessagePowerSync.class,
        ID.incrementAndGet(), Side.CLIENT);
  }

}
