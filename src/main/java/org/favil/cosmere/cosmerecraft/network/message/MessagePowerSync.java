package org.favil.cosmere.cosmerecraft.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessagePowerSync implements IMessage {
  @Override
  public void fromBytes(ByteBuf buf) {

  }

  @Override
  public void toBytes(ByteBuf buf) {

  }

  public static class MessagePowerSyncHandler
      implements IMessageHandler<MessagePowerSync, IMessage> {
    @Override
    public IMessage onMessage(MessagePowerSync message, MessageContext ctx) {
      return null;
    }
  }
}
