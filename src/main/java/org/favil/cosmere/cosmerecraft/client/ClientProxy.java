package org.favil.cosmere.cosmerecraft.client;

import org.favil.cosmere.cosmerecraft.CosmereCraftMod;
import org.favil.cosmere.cosmerecraft.client.CommonProxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;

public class ClientProxy extends CommonProxy {
  
  private static final Minecraft mc = Minecraft.getMinecraft();

  @Override
  void registerHandlers() {
    super.registerHandlers();
    
    // MinecraftForge.EVENT_BUS.register(new KeyboardHandler());
  }

  @Override
  public void registerItemRenderer(Item item, int meta, String id) {
    ModelLoader.setCustomModelResourceLocation(item, meta,
        new ModelResourceLocation(CosmereCraftMod.MODID + ":" + id, "inventory"));
  }
  
  
}
