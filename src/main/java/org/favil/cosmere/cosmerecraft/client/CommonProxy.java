package org.favil.cosmere.cosmerecraft.client;

import org.favil.cosmere.cosmerecraft.CosmereCraftMod;
import org.favil.cosmere.cosmerecraft.items.ModItems;
import org.favil.cosmere.cosmerecraft.network.PacketHandler;
import org.favil.cosmere.cosmerecraft.world.ModWorldGeneration;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class CommonProxy {

  void registerHandlers() {
    CosmereCraftMod.logger.info("Registering handlers");

    // FMLCommonHandler.instance().bus().register(new Handler());
  }

  public EntityPlayer getPlayer(MessageContext context) {
    return context.getServerHandler().playerEntity;
  }

  public void preInit(FMLPreInitializationEvent e) {
    ModItems.preInit();
    GameRegistry.registerWorldGenerator(new ModWorldGeneration(), 3);
  }

  public void init(FMLInitializationEvent e) {
    ModItems.init();
    registerHandlers();

    PacketHandler.init();

  }

  public void postInit(FMLPostInitializationEvent e) {
    ModItems.postInit();
  }

  public void registerItemRenderer(Item itemBlock, int meta, String id) {}
}
