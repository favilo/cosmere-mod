package org.favil.cosmere.cosmerecraft;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.favil.cosmere.cosmerecraft.client.CommonProxy;

import com.google.common.base.Stopwatch;

import net.minecraftforge.common.ForgeVersion;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = CosmereCraftMod.MODID, version = CosmereCraftMod.VERSION, name = CosmereCraftMod.NAME,
    acceptedMinecraftVersions = "[1.10.2, 1.11]", dependencies = CosmereCraftMod.MOD_DEPENDENCIES)
public class CosmereCraftMod {
  public static final String MODID = "cosmerecraftmod";
  public static final String VERSION = "@VERSION@";
  public static final String NAME = "CosmereCraft";
  public static final String MOD_DEPENDENCIES =
      // require forge.
      "after:IC2;after:ImmersiveEngineering;after:TConstruct;" + "required-after:Forge@[12.18.3.2183,);";

  @Instance(MODID)
  public static CosmereCraftMod instance;

  @SidedProxy(clientSide = "org.favil.cosmere.cosmerecraft.client.ClientProxy",
      serverSide = "org.favil.cosmere.cosmerecraft.client.CommonProxy")
  public static CommonProxy proxy;


  public static final Logger logger = LogManager.getLogger(CosmereCraftMod.class.getSimpleName());

  @EventHandler
  public void preInit(FMLPreInitializationEvent event) {
    Stopwatch watch = Stopwatch.createStarted();

    proxy.preInit(event);
    logger.info("Pre-initialized after " + watch.elapsed(TimeUnit.MILLISECONDS) + "ms");
  }

  @EventHandler
  public void init(FMLInitializationEvent event) {
    Stopwatch watch = Stopwatch.createStarted();
    proxy.init(event);
    logger.info("Initialized after " + watch.elapsed(TimeUnit.MILLISECONDS) + "ms");
  }

  @EventHandler
  public void postInit(FMLPostInitializationEvent event) {
    Stopwatch watch = Stopwatch.createStarted();
    proxy.postInit(event);

    logger.info("Post-initialized after " + watch.elapsed(TimeUnit.MILLISECONDS) + "ms");
  }
}
