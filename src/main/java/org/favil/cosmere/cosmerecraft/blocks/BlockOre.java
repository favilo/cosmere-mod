package org.favil.cosmere.cosmerecraft.blocks;

import org.favil.cosmere.cosmerecraft.items.ItemOreDict;

import net.minecraft.block.material.Material;
import net.minecraftforge.oredict.OreDictionary;

public class BlockOre extends BlockBase implements ItemOreDict {

  public BlockOre(String name) {
    super(Material.ROCK, name);
    setHardness(3f);
    setResistance(5f);
  }

  @Override
  public void initOreDict() {
    OreDictionary.registerOre(name, this);
  }
}
