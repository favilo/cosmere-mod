package org.favil.cosmere.cosmerecraft.blocks;

import org.favil.cosmere.cosmerecraft.CosmereCraftMod;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemBlock;

public class BlockBase extends Block {

  String name;

  public BlockBase(Material materialIn, String name) {
    super(materialIn);
    this.name = name;
    
    setUnlocalizedName(name);
    setRegistryName(name);
  }
  
  public void registerItemModel(ItemBlock itemBlock) {
    CosmereCraftMod.proxy.registerItemRenderer(itemBlock, 0, name);
  }
}
