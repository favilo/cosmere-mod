package org.favil.cosmere.cosmerecraft.items;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.resources.I18n;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class ItemAllomancyPotion extends ItemBase implements ItemOreDict {

  private static final int DURATION_TICKS = 10; // The number of ticks it takes to drink the potion

  public ItemAllomancyPotion() {
    super("itemAllomancyPotion", 64);
    setHasSubtypes(true);
  }

  @Override
  public void initOreDict() {
    OreDictionary.registerOre(name, this);
  }

  @Override
  public EnumAction getItemUseAction(ItemStack stack) {
    return EnumAction.DRINK;
  }

  @Override
  public int getMaxItemUseDuration(ItemStack stack) {
    return DURATION_TICKS;
  }

  @Override
  @Nullable
  public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving) {
    if (entityLiving instanceof EntityPlayer
        && !((EntityPlayer) entityLiving).capabilities.isCreativeMode) {
      --stack.stackSize;
    }

    if (entityLiving instanceof EntityPlayer) {
      ((EntityPlayer) entityLiving).addStat(StatList.getObjectUseStats(this));
    }


    if (entityLiving instanceof EntityPlayer) {
      ((EntityPlayer) entityLiving).inventory
          .addItemStackToInventory(new ItemStack(Items.GLASS_BOTTLE));
    }

    return stack;
  }

  @Override
  public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn,
      EntityPlayer playerIn, EnumHand hand) {
    playerIn.setActiveHand(hand);
    return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStackIn);
  }

  @SideOnly(Side.CLIENT)
  @Override
  public void addInformation(ItemStack stack, EntityPlayer player, List<String> tooltip,
      boolean isAdvanced) {
    int bitmap = stack.getItemDamage();
    if (bitmap == 0) {
      tooltip.add(I18n.format("cosmerecraftmod.potion.tooltip.empty.desc"));
      return;
    }
    for (int i = 0; i < ModItems.ALL_METAL_NAMES.size(); i++) {
      if ((bitmap & (1 << i)) != 0) {
        tooltip.add(I18n.format("cosmerecraftmod.potion.tooltip."
            + ModItems.ALL_METAL_NAMES.get(i).toLowerCase() + ".desc"));
      }
    }
  }
}
