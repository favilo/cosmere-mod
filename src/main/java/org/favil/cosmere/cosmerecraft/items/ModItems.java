package org.favil.cosmere.cosmerecraft.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.favil.cosmere.cosmerecraft.CosmereCraftMod;
import org.favil.cosmere.cosmerecraft.blocks.BlockBase;
import org.favil.cosmere.cosmerecraft.blocks.BlockOre;
import org.favil.cosmere.cosmerecraft.items.recipes.PotionRecipe;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class ModItems {

  public static final List<String> METAL_STYLES =
      ImmutableList.of("ore", "dust", "dustTiny", "ingot");
  public static final List<String> ORE_NAMES = ImmutableList.of("Tin", "Copper", "Iron", "Zinc");
  public static final List<String> ALLOY_NAMES =
      ImmutableList.of("Pewter", "Bronze", "Steel", "Brass");
  public static final List<String> ALL_METAL_NAMES =
      ImmutableList.copyOf(Iterables.concat(ORE_NAMES, ALLOY_NAMES));
  
  public static Map<String, Item> itemMap =new HashMap<>();
  public static BiMap<String, ItemStack> itemStackBiMap = HashBiMap.create();
  public static Map<String, Block> blockMap = new HashMap<>();
  public static Set<String> createdMetals = new HashSet<>();  // Useful for recipes
  public static List<String> createdOres = new ArrayList<>();  // Used for world generation
  public static ItemFile itemFile;
  public static ItemAllomancyPotion itemAllomancyPotion;
  
  public static void preInit() {
    checkMetals(METAL_STYLES, ORE_NAMES);
    checkMetals(METAL_STYLES.subList(1, 4), ALLOY_NAMES);
    registerItems();
  }

  private static void registerItems() {
    itemFile = new ItemFile();
    GameRegistry.register(itemFile);
    itemFile.registerItemModel();
    itemAllomancyPotion = new ItemAllomancyPotion();
    GameRegistry.register(itemAllomancyPotion);
    itemAllomancyPotion.registerItemModel();
  }

  private static void registerCoalDust() {
    String name = "dustCoal";
    List<ItemStack> oreStack = OreDictionary.getOres(name);
    if (oreStack.size() == 0) {
      Item item = registerItem(new ItemOre(name).setCreativeTab(CreativeTabs.MATERIALS));
      itemMap.put(name, item);
      itemStackBiMap.put(name, new ItemStack(item, 1, 0));
    } else {
      itemMap.put(name, oreStack.get(0).getItem());
      itemStackBiMap.put(name, oreStack.get(0));

    }
  }

  private static void checkMetals(List<String> styles, List<String> metals) {
    for (String metalName : metals) {
      for (String style : styles) {
        String name = style + metalName;
        List<ItemStack> oreStack = OreDictionary.getOres(name);
        if (oreStack.size() > 0) {
          CosmereCraftMod.logger.info(name + " exists!");
          itemMap.put(name, oreStack.get(0).getItem());
          itemStackBiMap.put(name, oreStack.get(0));
          if (style.equals("dustTiny")) {
            oreStack.forEach(ore -> OreDictionary.registerOre("allomanticDustTiny", ore));
          }
        } else {
          CosmereCraftMod.logger.warn("Creating ore: " + name);
          if (name.equals("dustSteel")) {
            registerCoalDust();
          }
          Item item = registerMetal(name);
          itemMap.put(name, item);
          itemStackBiMap.put(name, new ItemStack(item, 1, 0));
        }
      }
    }
  }
  
  public static void init() {
    registerRecipes();
  }
  
  public static void postInit() {
    
  }
  
  private static Item registerMetal(String name) {
    createdMetals.add(name);
    if (name.startsWith("ore")) {
      Block block = new BlockOre(name).setCreativeTab(CreativeTabs.MATERIALS);
      blockMap.put(name, block);
      createdOres.add(name);
      return registerBlock(block);
    } else {
      return registerItem(new ItemOre(name).setCreativeTab(CreativeTabs.MATERIALS));
    }
  }
  
  private static void registerRecipes() {
    registerItemRecipes();
    registerMetalRecipes();
    registerAlloyRecipes();
  }

  private static void registerItemRecipes() {
    GameRegistry.addRecipe(
        new ShapedOreRecipe(itemFile, "M  ", " M ", "  S", 'M', "ingotIron", 'S', "stickWood"));
    GameRegistry.addRecipe(new PotionRecipe());
  }
  
  private static void registerMetalRecipes() {
    for (String metal : ALL_METAL_NAMES) {
      ItemStack tinyDust = getItemStackFromName("dustTiny" + metal);
      tinyDust.stackSize = 9;
      GameRegistry.addRecipe(new ShapelessOreRecipe(
          tinyDust, "dust" + metal));
      GameRegistry.addRecipe(new ShapedOreRecipe(getItemStackFromName("dust" + metal),
          "MMM", "MMM", "MMM", 'M', "dustTiny" + metal));
      GameRegistry.addSmelting(getItemStackFromName("dust" + metal),
          getItemStackFromName("ingot" + metal), 0.7f);
      if (ORE_NAMES.contains(metal)) {
        GameRegistry.addSmelting(getItemStackFromName("ore" + metal),
            getItemStackFromName("ingot" + metal), 0.7f);
      }
      GameRegistry.addRecipe(new ShapelessOreRecipe(getItemStackFromName("dust" + metal),
          "ingot" + metal, new ItemStack(itemFile, 1, OreDictionary.WILDCARD_VALUE)));
    }
  }

  private static void registerAlloyRecipes() {
    String name = "dustPewter";
    ItemStack pewterDust = getItemStackFromName(name);
    pewterDust.stackSize = 4;
    GameRegistry.addRecipe(
        new ShapelessOreRecipe(pewterDust, "dustTin", "dustTin", "dustTin", "dustCopper"));

    ItemStack dustBronze = getItemStackFromName("dustBronze");
    dustBronze.stackSize = 4;
    GameRegistry.addRecipe(
        new ShapelessOreRecipe(dustBronze, "dustCopper", "dustCopper", "dustCopper", "dustTin"));

    ItemStack dustBrass = getItemStackFromName("dustBrass");
    dustBrass.stackSize = 3;
    GameRegistry
        .addRecipe(new ShapelessOreRecipe(dustBrass, "dustCopper", "dustCopper", "dustZinc"));

    if (createdMetals.contains("dustSteel")) {
      ItemStack dustSteel = getItemStackFromName("dustSteel");
      dustSteel.stackSize = 3;
      GameRegistry.addRecipe(new ShapelessOreRecipe(dustSteel, "dustIron", "dustIron", "dustCoal"));
      GameRegistry.addRecipe(new ShapelessOreRecipe(getItemStackFromName("dustCoal"), Items.COAL,
          new ItemStack(itemFile, 1, OreDictionary.WILDCARD_VALUE)));
      
    }
  }

  public static ItemStack getItemStackFromName(String name) {
    return itemStackBiMap.get(name).copy();
  }
  
  public static String getNameFromItemStack(ItemStack stack) {
    return itemStackBiMap.inverse().get(stack);
  }

  private static <T extends Item> T registerItem(T item) {
    GameRegistry.register(item);
    if (item instanceof ItemBase) {
      ((ItemBase)item).registerItemModel();
    }
    if (item instanceof ItemOreDict) {
      ((ItemOreDict)item).initOreDict();
    }
    return item;
  }
  
  private static <T extends Block> ItemBlock registerBlock(T block) {
    ItemBlock itemBlock = new ItemBlock(block);
    itemBlock.setRegistryName(block.getRegistryName());
    
    GameRegistry.register(block);
    GameRegistry.register(itemBlock);
    
    if (block instanceof BlockBase) {
      ((BlockBase)block).registerItemModel(itemBlock);
    }
    if (block instanceof ItemOreDict) {
      ((ItemOreDict)block).initOreDict();
    }
    if (itemBlock instanceof ItemOreDict) {
      ((ItemOreDict)itemBlock).initOreDict();
    }
      
    
    return itemBlock;
  }
}
