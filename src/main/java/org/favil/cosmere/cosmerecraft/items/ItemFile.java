package org.favil.cosmere.cosmerecraft.items;

import net.minecraft.item.ItemStack;

public class ItemFile extends ItemBase {

  private static final int MAX_DAMAGE = 100;
  public ItemFile() {
    super("metalFile", 1);
  }

  @Override
  public int getMaxDamage(ItemStack stack) {
    return MAX_DAMAGE;
  }

  @Override
  public ItemStack getContainerItem(ItemStack itemStack) {
    itemStack.setItemDamage(itemStack.getItemDamage() + 1);
    return itemStack.copy();
  }

  @Override
  public boolean hasContainerItem(ItemStack stack) {
    return stack.getItemDamage() < MAX_DAMAGE;
  }
}