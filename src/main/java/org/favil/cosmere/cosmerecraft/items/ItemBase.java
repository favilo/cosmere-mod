package org.favil.cosmere.cosmerecraft.items;

import java.util.Collection;
import java.util.List;

import org.favil.cosmere.cosmerecraft.CosmereCraftMod;

import com.google.common.collect.ImmutableList;

import net.minecraft.item.Item;

public class ItemBase extends Item {

  protected final String name;
  protected final Collection<String> subNames;

  public ItemBase(String name, int stackSize, String... subNames) {
    this(name, stackSize, ImmutableList.copyOf(subNames));
  }

  public ItemBase(String name, int stackSize, Collection<String> subNames) {
    this.name = name;
    setUnlocalizedName(name);
    setRegistryName(name);
    this.subNames = ImmutableList.copyOf(subNames);
    setHasSubtypes(subNames != null && subNames.size() > 0);
    setMaxStackSize(stackSize);
  }

  public void registerItemModel() {
    CosmereCraftMod.proxy.registerItemRenderer(this, 0, name);
  }
}