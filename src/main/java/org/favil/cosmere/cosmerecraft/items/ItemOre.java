package org.favil.cosmere.cosmerecraft.items;

import net.minecraftforge.oredict.OreDictionary;

public class ItemOre extends ItemBase implements ItemOreDict {

  public ItemOre(String name) {
    super(name, 64);
  }

  @Override
  public void initOreDict() {
    OreDictionary.registerOre(name, this);
    if (name.startsWith("dustTiny")) {
      OreDictionary.registerOre("allomanticDustTiny", this);
    }
  }
}