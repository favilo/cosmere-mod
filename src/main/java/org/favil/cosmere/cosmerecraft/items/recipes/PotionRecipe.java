package org.favil.cosmere.cosmerecraft.items.recipes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.favil.cosmere.cosmerecraft.items.ModItems;

import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.oredict.OreDictionary;

public class PotionRecipe implements IRecipe {
  @Override
  public boolean matches(InventoryCrafting inv, World worldIn) {
    boolean bottleFound = false;
    Map<String, Boolean> metalsExist = new HashMap<>();
    List<ItemStack> allomanticDusts = OreDictionary.getOres("allomanticDustTiny");
    for (int i = 0; i < inv.getSizeInventory(); i++) {
      ItemStack slot = inv.getStackInSlot(i);
      if (slot == null) {
        continue;
      }
      if (OreDictionary.itemMatches(new ItemStack(Items.POTIONITEM, 0), slot, true)) {
        if (bottleFound) {
          // Only one bottle, please
          return false;
        } else {
          bottleFound = true;
          continue;
        }
      }
      for (ItemStack target : allomanticDusts) {
        if (OreDictionary.itemMatches(target, slot, true)) {
          if (metalsExist.getOrDefault(target, false)) {
            // Only one of each metal, please
            return false;
          }
          metalsExist.put(ModItems.getNameFromItemStack(target), true);
          break;
        }
      }

    }
    return bottleFound && metalsExist.values().contains(true);
  }

  @Override
  public ItemStack getCraftingResult(InventoryCrafting inv) {
    int bitMap = 0;
    for (int i = 0; i < inv.getSizeInventory(); i++) {
      ItemStack slot = inv.getStackInSlot(i);
      if (slot == null) {
        continue;
      }
      for (String metalName : ModItems.ALL_METAL_NAMES) {
        if (OreDictionary.containsMatch(true, OreDictionary.getOres("dustTiny" + metalName),
            slot)) {
          bitMap |= 1 << ModItems.ALL_METAL_NAMES.indexOf(metalName);
          break;
        }
      }
    }
    if (bitMap == 0) {
      return new ItemStack(Items.POTIONITEM, 0);
    }
    return new ItemStack(ModItems.itemAllomancyPotion, 1, bitMap);
  }

  @Override
  public int getRecipeSize() {
    return 0;
  }

  @Override
  public ItemStack getRecipeOutput() {
    return new ItemStack(ModItems.itemAllomancyPotion);
  }

  @Override
  public ItemStack[] getRemainingItems(InventoryCrafting inv) {
    return ForgeHooks.defaultRecipeGetRemainingItems(inv);
  }

}
